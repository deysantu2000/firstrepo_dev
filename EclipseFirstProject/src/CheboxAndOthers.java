



import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.Select;

public class CheboxAndOthers {

	public static void main(String[] args) throws InterruptedException {
		
		// setting up firefox browser for automation
		System.setProperty("webdriver.gecko.driver", "C:\\Selenium Downloaded\\geckodriver-v0.18.0-win32\\geckodriver.exe");
		FirefoxOptions options = new FirefoxOptions();
		options.setBinary("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"); //This is the location where you firefox exe is located11111111111111111111
		//Open Quikr.com
		WebDriver driver = new FirefoxDriver();
		driver.get("http://kolkata.quikr.com/");
		
		
		try {
//finding Job Tab
		List<WebElement> naviagationElement = driver.findElements(By.linkText("Jobs"));
		int no=naviagationElement.size();
		System.out.println("Number of Jobs" + no);
		for(int i=1;i<=no;i++) {
			String s=naviagationElement.get(i).getAttribute("title");
			try {
					if(s.equalsIgnoreCase("Jobs")){
						System.out.println("Jobs" + i);
						naviagationElement.get(i).click();
						break;
					}
				}catch(Exception e)	{
					System.out.println("error");
				}
		}
		
			Thread.sleep(5000);
			
			
//Finding and clicking on BPO/Telecaller link
		WebElement oLink2 =driver.findElement(By.linkText("BPO/  Telecaller"));
		oLink2.click();
	
		Thread.sleep(10000);
//Selecting a checkbox - domestic
		List<WebElement> checkBox = driver.findElements(By.name("subroles[]"));
		System.out.println("total number of checkboxes" + checkBox.size());
		for(int i=1;i<=checkBox.size();i++) {
			String name=checkBox.get(i).getAttribute("value");
			if(name.equalsIgnoreCase("domestic")) {
				System.out.println(i);
				if(!(checkBox.get(i).isSelected())) {
					checkBox.get(i).click();
					System.out.println(i);
					break;
				}
			}
		}
		}catch(Exception e) {
			System.out.println("Inside Exception");
			//driver.close();
		}
		//driver.close();
//Selecting Cook option from dropdown 	
		Thread.sleep(5000);
		List<WebElement> element = driver.findElements(By.className("dropdown"));
		//element.get(1).click();
		for(int i=1;i<=element.size();i++) {
			if (element.get(i).getAttribute("id").equalsIgnoreCase("jsHeaderRole")) {
				element.get(i).click();
				WebElement oLink = driver.findElement(By.linkText("Cook"));
				oLink.click();
				break;
			}
		}
		Thread.sleep(5000);
		try {
		List<WebElement> verificationText = driver.findElements(By.className("job-title mb5"));
		System.out.println("number found" + verificationText.size());
		for (int i=1;i<=verificationText.size();i++) {
			System.out.println("tiles are "+ verificationText.get(i).getAttribute("title").toString());
		if (verificationText.get(i).getAttribute("title").equalsIgnoreCase("Home cook home servant and restaurant cooks required for kolkata mumbai and delhi"))
			System.out.println("Found");
		}
// Cl11osinfg browser
		driver.close();
		}catch(Exception e) {
			driver.close();
		}
	}
}
